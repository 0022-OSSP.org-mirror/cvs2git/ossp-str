/*
**  str_version.c -- Version Information for OSSP str (syntax: C/C++)
**  [automatically generated and maintained by GNU shtool]
*/

#ifdef _STR_VERSION_C_AS_HEADER_

#ifndef _STR_VERSION_C_
#define _STR_VERSION_C_

#define STR_VERSION 0x00920C

typedef struct {
    const int   v_hex;
    const char *v_short;
    const char *v_long;
    const char *v_tex;
    const char *v_gnu;
    const char *v_web;
    const char *v_sccs;
    const char *v_rcs;
} str_version_t;

extern str_version_t str_version;

#endif /* _STR_VERSION_C_ */

#else /* _STR_VERSION_C_AS_HEADER_ */

#define _STR_VERSION_C_AS_HEADER_
#include "str_version.c"
#undef  _STR_VERSION_C_AS_HEADER_

str_version_t str_version = {
    0x00920C,
    "0.9.12",
    "0.9.12 (12-Oct-2005)",
    "This is OSSP str, Version 0.9.12 (12-Oct-2005)",
    "OSSP str 0.9.12 (12-Oct-2005)",
    "OSSP str/0.9.12",
    "@(#)OSSP str 0.9.12 (12-Oct-2005)",
    "$Id: OSSP str 0.9.12 (12-Oct-2005) $"
};

#endif /* _STR_VERSION_C_AS_HEADER_ */

