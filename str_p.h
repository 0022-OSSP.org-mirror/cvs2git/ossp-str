/*
**  OSSP str - String Handling
**  Copyright (c) 1999-2005 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 1999-2005 The OSSP Project <http://www.ossp.org/>
**
**  This file is part of OSSP str, a string handling and manipulation
**  library which can be found at http://www.ossp.org/pkg/lib/str/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  str_p.h: private API header
*/

#ifndef _STR_P_H_
#define _STR_P_H_

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdlib.h>     /* for malloc, etc. */
#include <string.h>     /* ... */

#include "str.h"
#include "str_pcre.h"

#if defined(HAVE_DMALLOC_H) && defined(WITH_DMALLOC)
#include "dmalloc.h"
#endif

/* explicit support for unsigned char based ctype stuff */
#define str_isalpha(c)  (isalpha(((unsigned char)(c))))
#define str_isdigit(c)  (isdigit(((unsigned char)(c))))
#define str_isxdigit(c) (isxdigit(((unsigned char)(c))))
#define str_islower(c)  (islower(((unsigned char)(c))))
#define str_tolower(c)  (tolower((unsigned char)(c)))

extern void *(*str_mem_alloc)(size_t);
extern void *(*str_mem_realloc)(void *, size_t);
extern void  (*str_mem_free)(void *);
extern void *(*str_mem_dup)(void *, size_t);
extern void *(*str_mem_set)(void *, int, size_t);
extern void *(*str_mem_move)(void *, const void *, size_t);
extern void *(*str_mem_rev)(void *, size_t);
extern void *(*str_mem_char)(const void *, int, size_t);
extern void *(*str_mem_mem)(const void *, size_t, const void *, size_t);
extern int   (*str_mem_cmp)(const void *, const void *, size_t);

typedef struct str_vformat_st str_vformat_t;
struct str_vformat_st {
    char *curpos;
    char *endpos;
    union {
        int i; long l;
        double d; void *p;
    } data[6];
    int (*flush)(struct str_vformat_st *);
    char *(*format)(
        struct str_vformat_st *,
        char *, char *, int *,
        char *, int, char *, int, va_list
    );
};

int str_vformat(str_vformat_t *, const char *, va_list);

#define str_ilen(n,s) \
do { \
    register const char *_s = (const char *)(s); \
    while (*_s++) \
        /*nop*/; \
    (n) = _s - (const char *)(s) - 1; \
} while(0)

#endif /* _STR_P_H_ */

